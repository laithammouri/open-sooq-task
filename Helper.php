<?php

/**
 * Helper that creates an instance, initialise the state, do the
 * calculation and return the result
 */

require_once "LevenshteinDistanceCalculator.php";
require_once "HammingDistanceCalculator.php";

class Helper {

    /**
     * @desc calculates Hamming Distance
     * @param string $firstString
     * @param string $secondString
     * 
     * @return int $result
     */
    public static function calculateHammingDistance(string $firstString, string $secondString) {
        $instance = HammingDistanceCalculator::getInstance();
        $instance->setA($firstString);
        $instance->setB($secondString);

        return $instance->calculate();
    }

    /**
     * @desc calculates Levenstein Distance
     * @param $firstString
     * @param $secondString
     * 
     * @return int $result
     */
    public static function calculateLevensteinDistance(string $firstString, string $secondString) {
        $instance = LevenshteinDistanceCalculator::getInstance();
        $instance->setA($firstString);
        $instance->setB($secondString);

        return $instance->calculate();
    }
}