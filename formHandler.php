<?php
/**
 * Gets the inputs from the web form and passes them to the helper
 */

require_once "Helper.php";

if($_POST["algorithm"] == "hamming") {
    $firstString = $_POST["string-a"];
    $secondString = $_POST["string-b"];

    var_dump(Helper::calculateHammingDistance($firstString, $secondString));
}

if($_POST["algorithm"] == "levenstein") {
    $firstString = $_POST["string-a"];
    $secondString = $_POST["string-b"];

    var_dump(Helper::calculateLevensteinDistance($firstString, $secondString));
}






