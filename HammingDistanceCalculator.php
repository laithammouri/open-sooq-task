<?php

/**
 * Class is responsible for calculating Hamming Distance between two strings
 */

class HammingDistanceCalculator {
    private $a;
    private $b;
    private static $instance = null;

    private function __construct() {

    }

    /**
     * @desc sets the first string
     * 
     * @param a
     * @return void
     */
    public function setA(string $a) {
        $this->a = $a;
    }

    /**
     * @desc sets the second string
     * 
     * @param b
     * @return void
     */
    public function setB(string $b) {
        $this->b = $b;
    }

    /**
     * @desc gets and instance of this class
     * 
     * @return HammingDistanceCalculator
     */
    public static function getInstance() {
        if(self::$instance == null) {
            self::$instance = new static;
        }

        return self::$instance;
    }

    /**
     * @desc calculates the distance
     * 
     * @return int distance 
     */
    public function calculate() {
        if(strlen($this->a) !== strlen($this->b)) {
            return false;
        }

        $i = 0; $count = 0; 
        while (isset($this->a[$i]) != '') 
        { 
            if ($this->a[$i] != $this->b[$i]) 
                $count++; 
            $i++; 
        } 
        return $count; 
    }
}
