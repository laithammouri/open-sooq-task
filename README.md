# Project Title

Task for Open Sooq that calculates Levenstein Distance and Hamming Distance

## Getting Started

Colne the project and follow the instructions below.

### Prerequisites

PHP7

## Running the Web Application

* cd into the project folder
* In your terminal run ```php -S localhost:8000```
* On your browser go to ```http://localhost:8000/``` 
* Fill in the inputs and click on the algorithm you wish to use.

## Running the Shell Scripts

* cd into the project folder
* In your terminal run ```php script.php```
* Follow the instructions
* Have fun :)

## Built With

* PHP7
* HTML5 

## Authors

* **Laith N. Al-Ammouri**
