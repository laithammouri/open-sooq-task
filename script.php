<?php

/**
 * Gets the inputs from the user and passes them to the helper
 */

require_once "Helper.php";

$alogrithms = [
    1 => "levenstein",
    2 => "hamming"
];

fwrite(STDOUT, "Please enter the first string\n");
$firstString = fgets(STDIN);

fwrite(STDOUT, "Please enter the second string\n");
$secondString = fgets(STDIN);

fwrite(STDOUT, "Please enter 1 for Levenstein or enter 2 for Hamming\n");

$choice = (int)fgets(STDIN);

if(!isset($alogrithms[$choice])){
    fwrite(STDOUT, "Please enter only 1 or 2\n");
    exit;
}

$algorithm = $alogrithms[$choice];

if($algorithm == "hamming") {
    $result = Helper::calculateHammingDistance($firstString, $secondString);
    if(!$result) {
        fwrite(STDOUT, "The two strings must have the same length\n");
        exit;
    }
    fwrite(STDOUT, "Result of Hamming distance is: $result\n");
    exit;
}

if($algorithm == "levenstein") {
    $result = Helper::calculateLevensteinDistance($firstString, $secondString);
    fwrite(STDOUT, "Result of Levenstein distance is: $result\n");
    exit;
}




