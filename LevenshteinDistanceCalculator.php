<?php

/**
 * Class is responsible for calculating Levenstein Distance between two strings
 */

class LevenshteinDistanceCalculator {
    private $a;
    private $b;
    private static $instance = null;

    private function __construct() {

    }
    /**
     * @desc sets the first string
     * 
     * @param a
     * @return void
     */
    public function setA(string $a){
        $this->a = $a;
    }

    /**
     * @desc sets the second string
     * 
     * @param b
     * @return void
     */
    public function setB(string $b) {
        $this->b = $b;
    }

    /**
     * @desc gets and instance of this class
     * 
     * @return LevenshteinDistanceCalculator
     */
    public static function getInstance() {
        if(self::$instance == null) {
            self::$instance = new static;
        }

        return self::$instance;
    }

    /**
     * @desc calculates the distance
     * 
     * @return int distance 
     */
    public function calculate() {
        $leftString = (strlen($this->a) > strlen($this->b)) ? $this->a : $this->b; 
        $rightString = (strlen($this->a) > strlen($this->b)) ? $this->b : $this->a; 
        $leftStringLength = strlen($leftString); 
        $rightStringLength = strlen($rightString);  
        if (($leftStringLength < $rightStringLength) && (strpos($rightString, $leftString) !== FALSE)) 
          return $rightStringLength - $leftStringLength; 
        else if (($rightStringLength < $leftStringLength) && (strpos($leftString, $rightString) !== FALSE)) 
          return $leftStringLength - $rightStringLength; 
        else { 
          $distance = range(1, $rightStringLength + 1); 
          for ($leftPosition = 1; $leftPosition <= $leftStringLength; ++$leftPosition) 
          { 
            $left = $leftString[$leftPosition - 1]; 
            $diagonal = $leftPosition - 1; 
            $distance[0] = $leftPosition; 
            for ($rightPosition = 1; $rightPosition <= $rightStringLength; ++$rightPosition) 
            { 
              $right = $rightString[$rightPosition - 1]; 
              $cost = ($right == $left) ? 0 : 1; 
              $newDiagonal = $distance[$rightPosition]; 
              $distance[$rightPosition] = 
                min($distance[$rightPosition] + 1, 
                    $distance[$rightPosition - 1] + 1, 
                    $diagonal + $cost); 
              $diagonal = $newDiagonal; 
            } 
          } 
          return $distance[$rightStringLength];
        } 
    }
}
