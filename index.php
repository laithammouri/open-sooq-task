<!DOCTYPE html>
<html>
    <head>
        <title>Open Souq Task</title>
    </head>

    <body>

        <form name = "calculate-distance" method = "POST" action = "formHandler.php">
            <fieldset>
                <legend>Calculate Distance:</legend>

                <input type="text" name = "string-a" placeholder="Insert String No. 1">
                <input type="text" name = "string-b" placeholder="Insert String No. 2">

                <button type="submit" name="algorithm" value="hamming">Claculate Hamming Distance</button>
                <button type="submit" name="algorithm" value="levenstein">Calculate Levenstein Distance</button>

            </fieldset>
        </form>

    </body>
</html>